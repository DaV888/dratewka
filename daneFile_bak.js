class Dane {
	constructor(){
		this.daneLokacji = `You are inside a brimstone mine;11.gif;rgb(235,211,64);e
You are at the entrance to the mine;12.gif;rgb(89,93,87);ew
A hill;13.gif;rgb(117,237,243);esw
Some bushes;14.gif;rgb(202,230,51);ew
An old deserted hut;15.gif;rgb(220,204,61);ew
The edge of a forest;16.gif;rgb(167,245,63);ew
A dark forest;17.gif;rgb(140,253,99);sw

A man nearby making tar;21.gif;rgb(255,190,99);es
A timber yard;22.gif;rgb(255,190,99);esw
You are by a roadside shrine;23.gif;rgb(167,245,63);nesw
You are by a small chapel;24.gif;rgb(212,229,36);ew
You are on a road leading to a wood;25.gif;rgb(167,245,63);esw
You are in a forest;26 i 65.gif;rgb(167,245,63);ew
You are in a deep forest;27 i 67.gif;rgb(140,253,99);nw

You are by the Vistula River;31.gif;rgb(122,232,252);ne
You are by the Vistula River;32.gif;rgb(140,214,255);nw
You are on a bridge over river;33.gif;rgb(108,181,242);ns
You are by the old tavern;34.gif;rgb(255,189,117);e
You are at the town's end;35.gif;rgb(255,190,99);nsw
You are in a butcher's shop;36.gif;rgb(255,188,102);s
You are in a cooper's house;37.gif;rgb(255,188,102);s

You are in the Wawel Castle;41.gif;rgb(255,176,141);e
You are inside a dragon's cave;42.gif;rgb(198,205,193);ew
A perfect place to set a trap;43.gif;rgb(255,176,141);nw
You are by the water mill;44.gif;rgb(255,190,99);e
You are at a main crossroad;45.gif;rgb(255,190,99);nesw
You are on a town street;46.gif;rgb(255,190,99);new
You are in a frontyard of your house;47.gif;rgb(255,190,99);nsw

null;null;null;null
null;null;null;null
null;null;null;null
You are by a swift stream;54.gif;rgb(108,181,242);e
You are on a street leading forest;55.gif;rgb(255,190,99);nsw
You are in a woodcutter's backyard;56.gif;rgb(255,190,99);s
You are in a shoemaker's house;57.gif;rgb(254,94,7);n

null;null;null;null
null;null;null;null
null;null;null;null
You are in a bleak funeral house;64.gif;rgb(254,194,97);e
You are on a path leading to the wood;26 i 65.gif;rgb(167,245,63);new
You are at the edge of a forest;66.gif;rgb(167,245,63);new
You are in a deep forest;27 i 67.gif;rgb(140,253,99);w`;
		this.items = [
			{"id": 10, "fullName": "a KEY","canUse": true, "name":"KEY"},
			{"id": 11, "fullName": "an AXE", "canUse":true,"name": "AXE"},
			{"id": 12, "fullName": "STICKS", "canUse": true, "name":"STICKS"},
			{"id": 13, "fullName": "sheeplegs","canUse": false ,"name":"sheeplegs"},
			{"id": 14, "fullName": "MUSHROOMS", "canUse": true, "name":"MUSHROOMS"},
			{"id": 15, "fullName": "MONEY","canUse":true, "name":"MONEY"},
			{"id": 16, "fullName": "a BARREL","canUse": true, "name":"BARREL"},
			{"id": 17, "fullName": "a sheeptrunk","canUse": false,"name":"sheeptrunk"},
			{"id": 18, "fullName": "BERRIES","canUse": true,"name":"BERRIES"},
			{"id": 19, "fullName": "WOOL","canUse": true,"name":"WOOL"},
			{"id": 20, "fullName": "a sheepskin","canUse": false,"name":"sheepskin"},
			{"id": 21, "fullName": "a BAG","canUse": true,"name":"BAG"},
			{"id": 22, "fullName": "a RAG","canUse": true, "name":"RAG"},
			{"id": 23, "fullName": "a sheephead","canUse": false,"name":"sheephead"},
			{"id": 24, "fullName": "a SPADE","canUse": true,"name":"SPADE"},
			{"id": 25, "fullName": "SULPHUR","canUse": true, "name":"SULPHUR"},
			{"id": 26, "fullName": "a solid poison", "canUse":false, "name":"solid poison"},
			{"id": 27, "fullName": "a BUCKET","canUse": true, "name":"BUCKET"},
			{"id": 28, "fullName": "TAR", "canUse":true, "name":"TAR"},
			{"id": 29, "fullName": "a liquid poison", "canUse":false,"name":"liquid poison"},
			{"id": 30, "fullName": "a dead dragon", "canUse":false, "name":"dead dragon"},
			{"id": 31, "fullName": "a STONE","canUse": true ,"name":"STONE"},
			{"id": 32, "fullName": "a FISH","canUse": true,"name":"FISH"},
			{"id": 33, "fullName": "a KNIFE","canUse": true,"name":"KNIFE"},
			{"id": 34, "fullName": "a DRAGONSKIN","canUse": true,"name":"DRAGONSKIN"},
			{"id": 35, "fullName": "a dragonskin SHOES", "canUse":true,"name":"SHOES"},
			{"id": 36, "fullName": "a PRIZE","canUse": true,"name":"PRIZE"},
			{"id": 37, "fullName": "a SHEEP","canUse": true,"name":"SHEEP"}
		]
		this.startItems = [
			{ "location": 13, "startItemId": 31 },
			{ "location": 15, "startItemId": 27 },
			{ "location": 17, "startItemId": 14 },
			{ "location": 23, "startItemId": 10 },
			{ "location": 27, "startItemId": 18 },
			{ "location": 32, "startItemId": 32 },
			{ "location": 44, "startItemId": 21 },
			{ "location": 55, "startItemId": 33 },
			{ "location": 64, "startItemId": 24 }
		]
		this.difrentLocationOfObject = []
	}
    
}
