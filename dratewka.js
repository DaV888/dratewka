/* eslint-disable default-case */
/* eslint-disable arrow-parens */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-undef */
/* eslint-disable linebreak-style */
function ready() {
    const dane = new Dane();
    class Localization {
        constructor(position, directionName = null) {
            this.transitionTime = 1000
            this.position = position;
            this.directionName = directionName
            this.allItems = dane.items;
            this.startItems = dane.startItems
            this.dependence = dane.dependenceLocation
            // this.assignedObjToLocationByUser = dane.difrentLocationOfObject
        }

        go() {
            console.log(this.position)
            console.log(this.position[3])
            document.getElementById("whereWeAre").innerHTML = this.position[0]
            let LocationList = 'You can Go:'
            if(this.position[3].search('w') > -1){
                LocationList+= " WEST,"
            }
            if(this.position[3].search('n') > -1){
                LocationList+= " NORTH,"
            }
            if(this.position[3].search('e') > -1){
                LocationList+= " EAST,"
            }
            if(this.position[3].search('s') > -1){
                LocationList+= " SOUTH,"
            }

            console.log(document.getElementsByTagName("label")[0].innerText)
            if(this.directionName != null){
                document.getElementById('inp').value = '';
                document.getElementsByTagName("label")[0].innerText = 'You are going ' + this.directionName + '...'
                document.getElementById('inp').style.display = 'none'
                setTimeout(()=>{this.goingTimeout()}, this.transitionTime);
            }

            LocationList = LocationList.substring(LocationList.length - 1, 0) + '.'
            document.getElementById('whereCanGo').innerHTML =LocationList

            document.getElementById("whatCarrying").innerHTML= 'You are carrying ' + Giera.carrying.name
            document.getElementById("whatSee").innerHTML= 'You see nothing'


            this.position = Giera.locationData[Giera.currPos.x][Giera.currPos.y];
            document.getElementById('image').src = `./data/img/`+this.position[1];
            document.getElementById('image').style.backgroundColor = this.position[2];

            this.compareItemsOnLocation()
            this.compassCover(this.position[3])
            
        }

        compareItemsOnLocation(){ //what you see
            var localizationId = this.position[1].substring(this.position[1].length - 4, 0)
            // var itemId = this.assignedObjToLocationByUser.filter(({location}) => localizationId == location)
            // if(itemId == undefined || itemId.length == 0)
                var itemId = this.startItems.filter(({location}) => localizationId == location)
            console.log(itemId)
            var whatSee = document.getElementById("whatSee")
            if(itemId != undefined && itemId.length != 0){
                whatSee.innerHTML= 'You see '
                itemId.forEach((el, i) =>{
                    console.log(el)

                    Giera.itemOnThisPosition[i] = this.allItems.find(({id}) => el.startItemId == id)
                    
                    if(Giera.itemOnThisPosition[i].name != null){
                        whatSee.innerHTML+= Giera.itemOnThisPosition[i].fullName+ ', '
                    }else{
                        whatSee.innerHTML= 'You see nothing'
                        Giera.itemOnThisPosition = []
                    }
                    console.log(Giera.itemOnThisPosition[i])
                })
                whatSee.innerHTML = whatSee.innerHTML.substring(whatSee.innerHTML.length - 2 , 0)+'.'
            }
            return Giera.itemOnThisPosition
        }
        compassCover(dirs = 'nesw'){
            document.getElementById("compass").innerHTML = ''
            if(dirs.search('w') < 0){
                var dv = document.createElement('div')
                dv.style.width = '14px'
                dv.style.height = '14px'
                dv.style.left = "14px"
                dv.style.top = "35px"
                document.getElementById("compass").appendChild(dv)
            }
            if(dirs.search('n') < 0){
                var dv = document.createElement('div')
                dv.style.width = '14px'
                dv.style.height = '14px'
                dv.style.left = "114px"
                document.getElementById("compass").appendChild(dv)
            }
            if(dirs.search('e') < 0){
                var dv = document.createElement('div')
                dv.style.width = '14px'
                dv.style.height = '14px'
                dv.style.left = "228px"
                dv.style.top = "33px"
                document.getElementById("compass").appendChild(dv)
            }
            if(dirs.search('s') < 0){
                var dv = document.createElement('div')
                dv.style.width = '14px'
                dv.style.height = '14px'
                dv.style.left = "120px"
                dv.style.top = "105px"
                document.getElementById("compass").appendChild(dv)
            }
        }

        goingTimeout(){
            document.getElementsByTagName("label")[0].innerText = 'What now?'
            document.getElementById('inp').style.display = 'inline-block'
        }

        take(itemArr){
            itemArr.shift()
            var itemString = itemArr.join(' ')
            console.log(itemString)
            this.compareItemsOnLocation()
            console.log(Giera.itemOnThisPosition)
            var isItem = Giera.itemOnThisPosition.find( ({name}) => name.toLowerCase() == itemString)
            console.log(isItem)
            if(isItem == null){
                document.getElementById('inp').style.display = 'none'
                document.getElementsByTagName("label")[0].innerText = "There isn't anything like that here"
                setTimeout(()=>{this.goingTimeout()}, this.transitionTime);
            } else if(isItem.canUse == false){
                document.getElementById('inp').style.display = 'none'
                document.getElementsByTagName("label")[0].innerText = "You can't carry it"
                setTimeout(()=>{this.goingTimeout()}, this.transitionTime);    
            } else if (Giera.carrying.name != 'nothing'){
                document.getElementById('inp').style.display = 'none'
                document.getElementsByTagName("label")[0].innerText = "You are carrying something"
                setTimeout(()=>{this.goingTimeout()}, this.transitionTime);
            } else if(isItem.canUse == true){
                Giera.carrying = isItem

                var localizationId = this.position[1].substring(this.position[1].length - 4, 0)
                var itemId = this.startItems.find(el => localizationId == el.location && el.startItemId == Giera.carrying.id)
                console.log(itemId)
                var index = this.startItems.indexOf(itemId)
                this.startItems.splice(index, 1)
                console.log(this.startItems)

                // itemId = this.startItems.find(el => localizationId == el.location  && el.startItemId == Giera.carrying.id)
                // index = this.startItems.indexOf(itemId)
                // this.startItems.splice(index, 1)
                
                document.getElementById("whatCarrying").innerHTML= 'You are carrying ' + Giera.carrying.name
                index = Giera.itemOnThisPosition.indexOf(Giera.carrying)
                Giera.itemOnThisPosition.splice(index, 1)
                this.compareItemsOnLocation()
                if(Giera.itemOnThisPosition != 0){
                    var see = 'You see '
                    Giera.itemOnThisPosition.forEach(({fullName})=> see += fullName+', ')
                    document.getElementById("whatSee").innerHTML = see.substring(see.length - 2 , 0)+'.'
                }else
                    document.getElementById("whatSee").innerHTML = "You see nothing"
    
            }
        }

        drop(itemArr){
            this.compareItemsOnLocation()
            itemArr.shift()
            var itemString = itemArr.join(' ')
            console.log(itemString)
            if(Giera.itemOnThisPosition.length >= 3){
                document.getElementById('inp').style.display = 'none'
                document.getElementsByTagName("label")[0].innerText = "You can't store any more here"
                setTimeout(()=>{this.goingTimeout()}, this.transitionTime);
            } else if(Giera.carrying.name.toLocaleLowerCase() != itemString){
                document.getElementById('inp').style.display = 'none'
                document.getElementsByTagName("label")[0].innerText = "You are not carrying it"
                setTimeout(()=>{this.goingTimeout()}, this.transitionTime);
            } else if (Giera.carrying.name != 'nothing'){
                console.log('dropuje')
                var localizationId = this.position[1].substring(this.position[1].length - 4, 0)
                // var juzJest = false
                // this.startItems.find((el) => {
                //     if(localizationId == el.location && Giera.carrying == el.id){
                //         console.log('tylko modyfikuje')
                //         el.location = localizationId
                //         el.startItemId = Giera.carrying.id
                //         juzJest = true
                //     }
                // })
                // if(!juzJest)
                this.startItems.push({location: parseInt(localizationId), startItemId: Giera.carrying.id})

                // itemId = this.startItems.find(el => localizationId == el.location  && el.startItemId == Giera.carrying.id)
                // index = this.startItems.indexOf(itemId)
                // this.startItems.splice(index, 1)

                // var index = {"location": localizationId, "startItemId": Giera.carrying.id}
                // this.startItems.splice(index, 1)
                // console.log(this.startItems)
                // document.getElementById("whatSee").innerHTML = "You see " + Giera.carrying.fullName
                this.compareItemsOnLocation()
                if(Giera.itemOnThisPosition != 0){
                    var see = 'You see '
                    Giera.itemOnThisPosition.forEach(({fullName})=> see += fullName+', ')
                    document.getElementById("whatSee").innerHTML = see.substring(see.length - 2 , 0)+'.'
                }else
                    document.getElementById("whatSee").innerHTML = "You see nothing"

                Giera.carrying = {}
                Giera.carrying = {name: 'nothing'}
                document.getElementById("whatCarrying").innerHTML= 'You are carrying ' + Giera.carrying.name
            }
        }
        use(itemArr){
            itemArr.shift()
            var itemString = itemArr.join(' ')
            this.compareItemsOnLocation()
            if(Giera.carrying.name.toLocaleLowerCase() != itemString){
                document.getElementById('inp').style.display = 'none'
                document.getElementsByTagName("label")[0].innerText = "You are not carrying it"
                setTimeout(()=>{this.goingTimeout()}, this.transitionTime);
            } else if (Giera.carrying.name != 'nothing'){
                console.log('używam')


                var localizationId = this.position[1].substring(this.position[1].length - 4, 0)
                console.log(Giera.carrying.id, Giera.carrying.name, localizationId)
                var canUseHere = null
                canUseHere = this.dependence.find(item => item.usingItemId == Giera.carrying.id && item.locationRequired == parseInt(localizationId))
                if(canUseHere){
                    console.log('tytej mozesz')
                    document.getElementById('inp').style.display = 'none'
                    // canUseHere.alert = "You are digging... (timeout) and digging... (timeout) That's enough sulphur for you"
                    // sam alert
                    var inter = null

                    var changeItemOnUse = () => {
                        if (canUseHere.partOfSheep)
                            dane.completed++;

                        console.log(Giera.carrying)
                        var item = this.allItems.find(({ id }) => (canUseHere.resultItem == id))
                        Giera.carrying.id = item.id
                        Giera.carrying.fullName = item.fullName
                        Giera.carrying.canUse = item.canUse
                        Giera.carrying.name = item.name

                        if (!Giera.carrying.canUse) {
                            this.drop(['d', Giera.carrying.name.toLowerCase()])
                            this.compareItemsOnLocation()
                        }

                        document.getElementById("whatCarrying").innerHTML = 'You are carrying ' + Giera.carrying.name
                        console.log(Giera.carrying)
                        if (dane.completed == 6)
                            setTimeout(() => {
                                item = this.allItems.find(({ id }) => (37 == id))
                                Giera.carrying.id = item.id
                                Giera.carrying.fullName = item.fullName
                                Giera.carrying.canUse = item.canUse
                                Giera.carrying.name = item.name
                                dane.completed = 0
                            }, 1500)

                    }


                    if (canUseHere.alert.search('(timeout)') > -1) {
                        var arAlert = canUseHere.alert.split("(timeout)")

                        document.getElementsByTagName("label")[0].innerText = arAlert[0]
                        var i = 1
                        var inter = setInterval( () => {
                            document.getElementById('inp').style.display = 'none'
                            document.getElementsByTagName("label")[0].innerText = arAlert[i]
                            console.log(arAlert[i])
                            console.log(i, arAlert)
                            if (i >= arAlert.length - 1 ) {
                                clearInterval(inter);
                                inter = null
                                setTimeout(()=>{this.goingTimeout()}, this.transitionTime);
                                changeItemOnUse()
                                return;
                            }
                            i++
                        }, 1500)

                    } else {
                        document.getElementsByTagName("label")[0].innerText = canUseHere.alert
                        setTimeout(()=>{this.goingTimeout()}, this.transitionTime);
                        changeItemOnUse()

                    }
                    // koniec alerta

                } else{
                    document.getElementById('inp').style.display = 'none'
                    document.getElementsByTagName("label")[0].innerText = "Nothing happened"
                    setTimeout(()=>{this.goingTimeout()}, this.transitionTime);
                    
                }

                
                // this.startItems.push({location: parseInt(localizationId), startItemId: Giera.carrying.id})

                // this.compareItemsOnLocation()
                // if(Giera.itemOnThisPosition != 0){
                //     var see = 'You see '
                //     Giera.itemOnThisPosition.forEach(({fullName})=> see += fullName+', ')
                //     document.getElementById("whatSee").innerHTML = see.substring(see.length - 2 , 0)+'.'
                // }else
                //     document.getElementById("whatSee").innerHTML = "You see nothing"

                // Giera.carrying = {}
                // Giera.carrying = {name: 'nothing'}
                // document.getElementById("whatCarrying").innerHTML= 'You are carrying ' + Giera.carrying.name
            }
        }

        goss() {
            var toPrint = 'The  woodcutter lost  his home key...<br>\
            The butcher likes fruit... The cooper<br>\
            is greedy... Dratewka plans to make a<br>\
            poisoned  bait for the dragon...  The<br>\
            tavern owner is buying food  from the<br>\
            pickers... Making a rag from a bag...<br>\
            Press any key'
            document.getElementById("whereCanGo").style.display = 'none'
            document.getElementById("whatSee").style.display = 'none'
            document.getElementById("whatCarrying").style.display = 'none'
            document.getElementsByTagName("label")[0].style.display = 'none'
            document.getElementById("inp").style.display = 'none'
            var goss = document.createElement('div')
            goss.width = '100%'
            goss.heigh = '100%'
            goss.style.lineHeight = '25px'
            document.getElementById('texts').appendChild(goss)
            goss.innerHTML = toPrint
            document.onkeydown = () => {
                document.getElementById('texts').removeChild(goss)
                document.getElementById("texts").text = ''
                document.getElementById("whereCanGo").style.display = 'block'
                document.getElementById("whatSee").style.display = 'block'
                document.getElementById("whatCarrying").style.display = 'block'
                document.getElementsByTagName("label")[0].style.display = 'block'
                document.getElementById("inp").style.display = 'block'
                document.onkeydown = null
            }
        }

        vocabulary() {
            console.log('vac')
            var toPrint = 'NORTH or N, SOUTH or S<br>\
            WEST or W, EAST or E<br>\
            TAKE (object) or T (object)<br>\
            DROP (object) or D (object)<br>\
            USE (object) or U (object)<br>\
            GOSSIPS or G, VOCABULARY or V<br>\
            Press any key'
            document.getElementById("whereCanGo").style.display = 'none'
            document.getElementById("whatSee").style.display = 'none'
            document.getElementById("whatCarrying").style.display = 'none'
            document.getElementsByTagName("label")[0].style.display = 'none'
            document.getElementById("inp").style.display = 'none'
            var voc = document.createElement('div')
            voc.width = '100%'
            voc.heigh = '100%'
            voc.style.lineHeight = '25px'
            document.getElementById('texts').appendChild(voc)
            voc.innerHTML = toPrint
            document.onkeydown = () => {
                document.getElementById('texts').removeChild(voc)
                document.getElementById("texts").text = ''
                document.getElementById("whereCanGo").style.display = 'block'
                document.getElementById("whatSee").style.display = 'block'
                document.getElementById("whatCarrying").style.display = 'block'
                document.getElementsByTagName("label")[0].style.display = 'block'
                document.getElementById("inp").style.display = 'block'
                document.onkeydown = null
            }
        }
    }




    const Giera = {
        start() {
            this.hejnal()
            console.log('init');
            // this.items = JSON.parse(this.readTextFile('/data/items.json'));
            this.items = dane.items
            this.locationData = this.converDataLocationtoArray();
            console.log(this.locationData);
            this.bigChars = true
            this.alwaysFocus();
            this.onKeyDownMove();
            firstLocalization = new Localization(this.locationData[this.currPos.x][this.currPos.y])
            firstLocalization.go()
            // document.getElementById('inp').style.textTransform = "uppercase"
        },

        items: {}, // items.json
        locationData: [], // dane_lokacji_1.txt
        currPos: { x: 3, y: 6 },
        carrying: {name: 'nothing'},
        itemOnThisPosition: [],

        // readTextFile(path) {
        //     const rawFile = new XMLHttpRequest();
        //     rawFile.open('GET', path, false);
        //     let allText;
        //     rawFile.onreadystatechange = () => {
        //         if (rawFile.readyState === 4) {
        //             if (rawFile.status === 200 || rawFile.status === 0) {
        //                 allText = rawFile.responseText;
        //             }
        //         }
        //     };
        //     rawFile.send(null);
        //     return allText;
        // },
        hejnal(){
            var over = document.getElementById('overlay')
            over.style.display = 'block'

            setTimeout(()=>{
                over.children[0].src = "data/w2.jpg"

                setTimeout(()=>{
                    over.children[0].src = "data/w3.jpg"

                    setTimeout(()=>{
                        over.innerHTML = ''
                        over.style.display = 'none'
                    }, 1500)

                }, 1500)
            }, 500)

        },
        converDataLocationtoArray() {
            locationDataRaw = dane.daneLokacji;
            const akapit = locationDataRaw.split('\n\n');
            const wartosc = [];
            akapit.forEach((el1, i) => {
                wartosc[i] = [];
                el1.split('\n').forEach((el2, j) => {
                    wartosc[i][j] = [];
                    el2.split(';').forEach((el3, l) => {
                        wartosc[i][j][l] = el3;
                    });
                });
            });
            return wartosc;
        },
        alwaysFocus() {
            document.addEventListener('keydown', () => {
                document.getElementsByTagName('input')[0].focus();
            });
        },
        submitEvents(position) {
            console.log('zaczynamy')
            this.position = position;
            console.log(position);
            const text = document.getElementById('inp').value.toLowerCase();
            console.log(document.getElementById('inp').value);


            if ((text === 'w' || text === 'west') && position[3].search('w') > -1) {
                this.currPos.y -= 1;
                this.position = this.locationData[this.currPos.x][this.currPos.y];
                const localization = new Localization(this.position, 'WEST');
                localization.go();
            } else if ((text === 'n' || text === 'north') && this.position[3].search('n') > -1) {
                this.currPos.x -= 1;
                this.position = this.locationData[this.currPos.x][this.currPos.y];
                const localization = new Localization(this.position, 'NORTH');
                localization.go();
            } else if ((text === 's' || text === 'south') && this.position[3].search('s') > -1) {
                this.currPos.x += 1;
                this.position = this.locationData[this.currPos.x][this.currPos.y];
                const localization = new Localization(this.position, 'SOUTH');
                localization.go();
            } else if ((text === 'e' || text === 'east') && this.position[3].search('e') > -1) {
                this.currPos.y += 1;
                this.position = this.locationData[this.currPos.x][this.currPos.y];
                const localization = new Localization(this.position, 'EAST');
                localization.go();
            } else if(text.split(' ')[0] === 't' || text.split(' ')[0] === 'take'){
                console.log('podnosisz'+ text.split(' ')[1])
                const location = new Localization(this.position, null)
                location.take(text.split(' '))
            } else if(text.split(' ')[0] === 'd' || text.split(' ')[0] === 'drop'){
                const location = new Localization(this.position, null)
                location.drop(text.split(' '))
            } else if(text.split(' ')[0] === 'u' || text.split(' ')[0] === 'use'){
                const location = new Localization(this.position, null)
                location.use(text.split(' '))
            }else if(text === 'g' || text === 'gossips'){
                const location = new Localization(this.position, null)
                location.goss()
            } else if (text != '') {
                const location = new Localization(this.position, null)
                location.vocabulary()
            }

            document.getElementById('inp').value = '';
        },

        onKeyDownMove() {
            let position = this.locationData[this.currPos.x][this.currPos.y];
            document.getElementById('image').style.backgroundColor = position[2];
            console.log(position);
            document.addEventListener('keydown', e => {
                e.preventDefault();
                position = this.locationData[this.currPos.x][this.currPos.y];
                console.log(position);
                console.log(e);
                switch (e.keyCode) {
                case 37: // przemieszanie na W
                    if (position[3].search('w') > -1) {
                        // console.log('mozna w lewo');
                        // console.log(this.locationData[this.currPos.x][this.currPos.y]);
                        // position = this.locationData[this.currPos.x][this.currPos.y];
                        // document.getElementById('image').src = `./data/img/${position[1]}`;
                        // document.getElementById('image').style.backgroundColor = position[2];
                        this.currPos.y -= 1;
                        position = this.locationData[this.currPos.x][this.currPos.y];
                        const localization = new Localization(position, 'WEST');
                        localization.go();
                    }
                    break;
                case 38: // przemieszanie na N
                    if (position[3].search('n') > -1) {
                        this.currPos.x -= 1;
                        position = this.locationData[this.currPos.x][this.currPos.y];
                        const localization = new Localization(position, 'NORTH');
                        localization.go();
                    }
                    break;
                case 39: // przemieszanie na E
                    if (position[3].search('e') > -1) {
                        this.currPos.y += 1;
                        position = this.locationData[this.currPos.x][this.currPos.y];
                        const localization = new Localization(position, 'EAST');
                        localization.go();
                    }
                    break;
                case 40: // przemieszanie na S
                    if (position[3].search('s') > -1) {
                        this.currPos.x += 1;
                        position = this.locationData[this.currPos.x][this.currPos.y];
                        const localization = new Localization(position, 'SOUTH');
                        localization.go();
                    }
                    break;
                case 13: // kliknięcie ENTER
                    this.submitEvents(position);
                    break;

                case 8:
                    document.getElementById('inp').value = document.getElementById('inp').value.substring(0, document.getElementById('inp').value.length - 1);
                    break;
                case 16: // Klinięcie SHIFT
                    // var sizeCase = document.getElementById('inp').style.textTransform
                    // if(sizeCase == 'lowercase'){
                    //     document.getElementById('inp').style.textTransform = 'uppercase'
                    // }else if(sizeCase == 'uppercase'){
                    //     document.getElementById('inp').style.textTransform = 'lowercase'
                    // }
                    this.bigChars = !this.bigChars
                    break;
                default:
                    if((e.keyCode > 64 && e.keyCode < 91) || /*(e.keyCode > 96 && e.keyCode < 123) ||*/ e.keyCode == 32){
                        if(this.bigChars){
                            document.getElementById('inp').value += e.key.toUpperCase()
                        }else{
                            document.getElementById('inp').value += e.key.toLowerCase()
                        }
                        console.log(e.key)
                    }
                    break;
                }
            });
        },
    };

    Giera.start();
}
document.addEventListener('DOMContentLoaded', ready);

// console.log(JSON.parse(readTextFile('materialy_dratewka/items.json')));
// console.log(JSON.parse(readTextFile('materialy_dratewka/items.json')));

// https://kursjs.pl/kurs/es6/class.php
// https://javascript.info/class
// https://www.sitepoint.com/es6-default-parameters/
